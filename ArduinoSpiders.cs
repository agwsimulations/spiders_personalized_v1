﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System.Threading; // threading for arduino. this way we dont need to stop running the game until something comes from arduino
using System.IO;

public class ArduinoSpiders : MonoBehaviour {

    public GameObject spiderPrefab;
    public Transform playerTransform;
    public Transform centerAnchorTransform;
    public TextMesh ArduinoValueTextMesh;

    [Header("Please tick only one of the 3 options:")]

    //[Header("  Set these to show spiders if value > a number")]
    private bool showIfGreaterThanNumber;
    private int numGreaterThan;

    //[Header("  Set these to show spiders if value < a number")]
    private bool showIfLessThanNumber;
    private int numLessThan;

    //[Header("  Set these to show spiders if value in range [min,max]")]
    private bool showInARange;
    private int numRangeMin;
    private int numRangeMax;

    [Header("This is the inital value from arduino")]
    public float InitialArduinoValue = 0f; // measure 100 arduino values at the start and find the average

    [Header("Desired stage")]
    public bool Stage1 = false;
    public bool Stage2 = false;
    public bool Stage3 = true;

    [Header("Patient stage")]
    public string PatientStage = "None";

    bool keeprunning = true;
    System.IO.Ports.SerialPort port; // arduino port

    /*
     * Arduino is sending values at 100Hz (baud rate 9600)
     */
    private int i = 0;
    private float arduino_output;
    private float last_value;
    private System.UInt64 counter = 0;
    /*
     * Some useful numbers
     */
    float user_eda_normalized = -100f;
    float desired_eda_normalized = -100f;
    float correction = -100f;

    StreamWriter stream; // Where we write some of the arduino output
    StreamWriter spiderstream; // Where we write info on spiders


    // Initialization
    void Start () {
        if ((port = new System.IO.Ports.SerialPort("COM5", 9600)) != null)
        {
            port.Close();
            port.Open();
            UnityEngine.Debug.Log("Port is set!");
        }
        else
        {
            UnityEngine.Debug.LogError("Error: Could not set port for arduino");
        }
               
        /*
         * Read any possible previous values that have been stored in Arduino.
         * After this, we start reading values in real time.
         */
        bool stopreading = false;
        //var out_stopwatch = new Stopwatch();
        //out_stopwatch.Start();
        //UnityEngine.Debug.Log("Cleaning at milliseconds: 0");
        while (!stopreading)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            if (port != null)
            {
                port.ReadLine();
            }
            stopwatch.Stop();
            long elapsed_time = stopwatch.ElapsedMilliseconds;

            if (elapsed_time > 5) // if more than 3 milliseconds
            {
                stopreading = true;
            }
        }
        //out_stopwatch.Stop();
        //UnityEngine.Debug.Log("Cleaning ended at milliseconds " + out_stopwatch.ElapsedMilliseconds);

        //out_stopwatch.Start();
        //UnityEngine.Debug.Log("Reading 200 values at mliliseconds: " + out_stopwatch.ElapsedMilliseconds);
        // Find an average of arduino value for 100 values
        if (port != null)
        {
            for (int i = 1; i <= 200; i++)
            {
                string line = port.ReadLine();
                float tmp;
                if (!(float.TryParse(line, out tmp)))
                {
                    UnityEngine.Debug.LogError("Error trying to read value from arduino. Read this from arduino: " + line);
                }
                else // parsed value correctly
                {
                    InitialArduinoValue = (InitialArduinoValue*(i-1) + tmp)/i; // running avg
                }
            }
        }
        //out_stopwatch.Stop();
        //UnityEngine.Debug.Log("Read the 200 values at milliseconds " + out_stopwatch.ElapsedMilliseconds);
        ParameterizedThreadStart pts = new ParameterizedThreadStart(ThreadedReadArduinoValue);
        Thread workerForOneRow = new System.Threading.Thread(pts);
        workerForOneRow.Start();
        UnityEngine.Debug.Log("Started the thread that reads arduino values!");

        var file = @"./myOutput.csv";
        var file2 = @"./spiderInfo.csv";
        stream = new StreamWriter(file, false);
        spiderstream = new StreamWriter(file2, false);
        //stream = File.CreateText(file);
        stream.WriteLine("Counter,Raw_EDA,Normalized_EDA,Correction,Patient_State,Desired_State");
        spiderstream.WriteLine("Counter,Timestamp,Spider_Scale,Spider_Force,Spider_Force_y,Distance");
    }

    // Obvious update function
    void Update()
    {
        i++;

        if (i % 120 == 1)
        {
            SpawnAndDestroy(spiderPrefab, 5f);
        }

        if (arduino_output >= 0.95f * InitialArduinoValue)
        {
            PatientStage = "Stage 1";
        }
        else if (arduino_output >= 0.8f * InitialArduinoValue)
        {
            PatientStage = "Stage 2";
        }
        else // less than 0.7f
        {
            PatientStage = "Stage 3";
        }
        
        if (i % 31 == 0)
        {
            ArduinoValueTextMesh.text = "User's value: " + arduino_output.ToString("000.0");
        }
        i = i % 120; // the number 120 pretty much shows how often spiders spawn. lower number = faster spawns. bigger number = slower spawns.
    }

    /*
    // Obvious update function (but a backup)
    void Update2()
    {
        i++;

        if (Stage3 && i == 1)
        {
            SpawnAndDestroyStage3(spiderPrefab, 5f); // 5 is the number (in seconds) for how long the spiders stay in the stage
        }
        else if (Stage2 && (i % 45 == 0))
        {
            SpawnAndDestroyStage2(spiderPrefab, 3f);
        }
        else if (Stage1 && i == 1)
        {
            SpawnAndDestroyStage1(spiderPrefab, 5f);
        }

        if (arduino_output >= 0.9f * InitialArduinoValue)
        {
            PatientStage = "Stage 1";
        }
        else if (arduino_output >= 0.7f * InitialArduinoValue)
        {
            PatientStage = "Stage 2";
        }
        else
        {
            PatientStage = "Stage 3";
        }

        if (i % 31 == 0)
        {
            ArduinoValueTextMesh.text = "arduino value: " + arduino_output.ToString("000.0");
        }
        i = i % 120; // the number 120 pretty much shows how often spiders spawn. lower number = faster spawns. bigger number = slower spawns.
    }
    */

    /*
    private void SpawnAndDestroyStage3(GameObject prefab, float waitTime)
    {
        // pick random point distance 2 meters from user
        // rotate spider so it is watching user
        // throw it towards him
        Vector3 pos = Random.insideUnitCircle.normalized;
        pos *= 2f; // 2 meters from user
        pos.z = pos.y;
        pos.y = -0.35f; // spawns at some height from the ground

        Vector2 me, it;
        me.x = centerAnchorTransform.position.x - playerTransform.position.x;
        me.y = centerAnchorTransform.position.z - playerTransform.position.z;
        
        it.x = pos.x;
        it.y = pos.z;

        Vector3 spiderSpawnPosition = new Vector3(me.x + it.x, pos.y, me.y + it.y + 2f);
        
        // instantiate a spider and make it look towards the camera
        GameObject obj = Instantiate(prefab, spiderSpawnPosition, Quaternion.identity);
        obj.transform.LookAt(playerTransform, transform.up);
        obj.transform.Rotate(new Vector3(-70f, 0f, 0f));
        obj.transform.Rotate(new Vector3(0f, 0f, -90f));

        Rigidbody rb = obj.GetComponent<Rigidbody>();
        Vector3 startpoint, endpoint;
        startpoint.x = obj.transform.position.x;
        startpoint.y = 0f;
        startpoint.z = obj.transform.position.z;
        endpoint.x = playerTransform.position.x;
        endpoint.y = 0f;
        endpoint.z = playerTransform.position.z;

        Vector3 force; // apply a force when the spider spawns, so it jumps near and towards the user (but with a little bit of randomness)
        force.x = Random.Range(endpoint.x - startpoint.x - 1f, endpoint.x - startpoint.x +1f) * 0.025f;
        force.z = Random.Range(endpoint.z - startpoint.z - 1f, endpoint.z - startpoint.z + 1f) * 0.025f;
        force.y = Random.Range(15f, 25f);

        force.y = 0.1f;
        rb.AddForce(force);

        Destroy(obj, waitTime);
    }
    */

    /*
    private void SpawnAndDestroyStage2(GameObject prefab, float waitTime)
    {
        // pick random point distance 2 meters from user
        // rotate spider so it is watching user
        // throw it towards him
        Vector3 pos = Random.insideUnitCircle.normalized;
        pos *= 1f; // 1 meter from user
        pos.z = pos.y;
        pos.y = -0.15f; // spawns at some height from the ground

        Vector2 me, it;
        me.x = centerAnchorTransform.position.x - playerTransform.position.x;
        me.y = centerAnchorTransform.position.z - playerTransform.position.z;

        it.x = pos.x;
        it.y = pos.z;

        Vector3 spiderSpawnPosition = new Vector3(me.x + it.x, pos.y, me.y + it.y + 2f);

        // instantiate a spider and make it look towards the camera
        GameObject obj = Instantiate(prefab, spiderSpawnPosition, Quaternion.identity);
        obj.transform.LookAt(playerTransform, transform.up);
        obj.transform.Rotate(new Vector3(-70f, 0f, 0f));
        obj.transform.Rotate(new Vector3(0f, 0f, -90f));

        Destroy(obj, waitTime);
    }
    */

    /*
    private void SpawnAndDestroyStage1(GameObject prefab, float waitTime)
    {
        // pick random point distance 2 meters from user
        // rotate spider so it is watching user
        // throw it towards him
        Vector3 pos = Random.insideUnitCircle.normalized;
        pos *= 2f; // 2 meters from user
        pos.z = pos.y;
        pos.y = -0.15f; // with this it spawns a bit over the ground

        Vector2 me, it;
        me.x = centerAnchorTransform.position.x - playerTransform.position.x;
        me.y = centerAnchorTransform.position.z - playerTransform.position.z;

        it.x = pos.x;
        it.y = pos.z;

        Vector3 spiderSpawnPosition = new Vector3(me.x + it.x, pos.y, me.y + it.y + 2f);

        // instantiate a spider and make it look towards the camera
        GameObject obj = Instantiate(prefab, spiderSpawnPosition, Quaternion.identity);
        obj.transform.LookAt(playerTransform, transform.up);
        obj.transform.Rotate(new Vector3(-70f, 0f, 0f));
        obj.transform.Rotate(new Vector3(0f, 0f, -90f));

        Destroy(obj, waitTime);
    }
    */

    private void SpawnAndDestroy(GameObject prefab, float waitTime)
    {
        // pick random point distance 2 meters from user
        // rotate spider so it is watching user
        // throw it towards him
        Vector3 pos = Random.insideUnitCircle.normalized;
        float temp_correction = correction; // just in case it changes from the other thread while we are in this one
        float temp_desired_eda_normalized = desired_eda_normalized;

        pos *= (2f * (1f + temp_correction)); // 2 meters from user
        pos.z = pos.y;
        pos.y = -0.35f - (temp_correction / 2.5f); // spawns at some height from the ground (number belongs in [-0.53, -0.17])

        Vector2 me, it;
        me.x = centerAnchorTransform.position.x - playerTransform.position.x;
        me.y = centerAnchorTransform.position.z - playerTransform.position.z;

        it.x = pos.x;
        it.y = pos.z;

        Vector3 spiderSpawnPosition = new Vector3(me.x + it.x, pos.y, me.y + it.y + 2f); // +2f because for some reason the spiders spawned 2 meters off on the Z axis, with this we are at the center of the circle now

        // instantiate a spider and make it look towards the camera
        GameObject obj = Instantiate(prefab, spiderSpawnPosition, Quaternion.identity);
        obj.transform.LookAt(playerTransform, transform.up);
        obj.transform.Rotate(new Vector3(-70f, 0f, 0f));
        obj.transform.Rotate(new Vector3(0f, 0f, -90f));

        Rigidbody rb = obj.GetComponent<Rigidbody>();
        Vector3 startpoint, endpoint;
        startpoint.x = obj.transform.position.x;
        startpoint.y = 0f;
        startpoint.z = obj.transform.position.z;
        endpoint.x = playerTransform.position.x;
        endpoint.y = 0f;
        endpoint.z = playerTransform.position.z;

        Vector3 force; // apply a force when the spider spawns, so it jumps near and towards the user (but with a little bit of randomness)
        force.x = Random.Range(endpoint.x - startpoint.x - 1f, endpoint.x - startpoint.x + 1f) * (0.001f + (0.004f / (temp_desired_eda_normalized * temp_desired_eda_normalized * temp_desired_eda_normalized))) * (1f - (2f * temp_correction));
        force.z = Random.Range(endpoint.z - startpoint.z - 1f, endpoint.z - startpoint.z + 1f) * (0.001f + (0.004f / (temp_desired_eda_normalized * temp_desired_eda_normalized * temp_desired_eda_normalized))) * (1f - (2f * temp_correction));
        force.y = Random.Range(0.01f, 0.08f)*(1f - temp_correction);
        
        rb.AddForce(force);
        obj.transform.localScale *= (1f / (temp_desired_eda_normalized * temp_desired_eda_normalized));

        float force_y = force.y; // just so i can print magnitude only one x and z
        force.y = 0f; // but i also want to write the force's y in the .csv

        spiderstream.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}",
        counter.ToString(), // Counter
            System.DateTime.Now.ToString("HH:mm:ss").ToString(), // Timestamp
            (1f / (temp_desired_eda_normalized * temp_desired_eda_normalized)).ToString(), // Spider_Scale
            force.magnitude.ToString(), // Spider_Force
            force_y.ToString(), // Spider_Force_y
            (Vector2.Distance(me, it)).ToString())); // Distance

        Destroy(obj, waitTime);
    }
    /*
     * Runs a thread in parallel to the main program, that reads the value from arduino
     */
    private void ThreadedReadArduinoValue(object threadParamsVar)
    {
        if(port != null)
        {
            while (keeprunning)
            {
                last_value = arduino_output;
                string line = port.ReadLine();
                if (!(float.TryParse(line, out arduino_output))) // Error case
                {
                    UnityEngine.Debug.LogError("Error trying to read value from arduino. Read this from arduino: " + line);
                }
                else // Successful case
                {
                    user_eda_normalized = arduino_output / InitialArduinoValue;
                    if (user_eda_normalized > 1f)
                    {
                        user_eda_normalized = 1f;
                    }

                    if (Stage3)
                    {
                        desired_eda_normalized = 0.55f;
                    }
                    else if (Stage2)
                    {
                        desired_eda_normalized = 0.85f;
                    }
                    else // can only be in this case if the doctor hasnt picked a desired stage yet
                    {
                        desired_eda_normalized = 0.98f;
                    }

                     correction = desired_eda_normalized - user_eda_normalized; // belongs in [-0.45 , 0.45]

                    int patientstate, desiredstate;
                    if (arduino_output >= 0.95f * InitialArduinoValue)
                    {
                        patientstate = 1;
                    }
                    else if (arduino_output >= 0.8f * InitialArduinoValue)
                    {
                        patientstate = 2;
                    }
                    else // less than 0.7f
                    {
                        patientstate = 3;
                    }

                    if (Stage3)
                    {
                        desiredstate = 3;
                    }
                    else if (Stage2)
                    {
                        desiredstate = 2;
                    }
                    else if (Stage1)
                    {
                        desiredstate = 1;
                    }
                    else
                    {
                        desiredstate = 0;
                    }

                    if (patientstate == desiredstate || desiredstate == 0)
                    {
                        stream.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", counter.ToString(), arduino_output.ToString(), (arduino_output / InitialArduinoValue).ToString(), "0.0", patientstate.ToString(), desiredstate.ToString()));
                    }
                    else
                    {
                        stream.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", counter.ToString(), arduino_output.ToString(), (arduino_output / InitialArduinoValue).ToString(), correction.ToString(), patientstate.ToString(), desiredstate.ToString()));
                    }
                    //UnityEngine.Debug.Log("Got arduino output: " + port.ReadLine()); // for debugging purposes
                }
                counter++;
            }
        }
        UnityEngine.Debug.Log("Arduino-reading-thread ended.");
    }

    void OnDestroy()
    {
        keeprunning = false;
        stream.Close();
        spiderstream.Close();
    }
}
